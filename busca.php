<?php header("Content-Type: text/html; charset=UTF-8",true) ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>Revesti Decorações e Móveis Planejados</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
     <!-- Required -->
    <link href="css/global-style.css" rel="stylesheet" type="text/css" media="screen">
    <link rel="icon" href="images/favicon.png" type="image/png">
</head>
<body>
<? include("controle/ConnFile.php"); ?>
<? include("menu.php"); ?>
<div class="wrapper">
	<!-- This section is only for demonstration purpose only. Just remove the div "divStyleSwitcher" -->
  <!-- Header: Logo and Main Nav -->
     

    </div>
</header>
     
    
    <section class="slice bg-3">
        <div class="w-section inverse">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="list-listings blog-list">

                            <?
                            $resultado = "";
                             $prod_query = mysqli_query($link, "SELECT idProdutoItem, Nome, Descricao FROM ProdutoItem WHERE Nome like '%".  $_GET["p"] ."%' or Descricao like  '%".  $_GET["p"] ."%'  ORDER BY  idProdutoItem  desc ");
							 while($prod = mysqli_fetch_array($prod_query))
                             {	
                                 $resultado = "S";
                            ?>
                            <li class="" style="width:100%;">
                                <div class="listing-body" style="width:100%;">
                                    <h3><a href="verproduto.php?k=<?=$prod['idProdutoItem'];?>"><?=$prod["Nome"]; ?></a></h3>
                                     <span class="list-item-info">
                                        Em <b>Produtos</b>.
                                    </span>
                                    <p>
                                      <?=substr(strip_tags( utf8_decode( $prod["Descricao"])),0,300);?>...
                                    </p>
                                   <p>
                                        <a href="verproduto.php?k=<?=$prod['idProdutoItem'];?>" class="btn btn-xs btn-two pull-right">Ver...</a>
                                    </p>
                                </div>
                            </li>
                            <?
                            }
                           ?>
                            

                            <? // OLD MÓVEIS PLANEJADOS
                            /*
                             $prod_query = mysql_query("SELECT idProdutoItem, Nome, Descricao FROM MoveisItem WHERE Nome like '%".  $_GET["p"] ."%' or Descricao like  '%".  $_GET["p"] ."%'  ORDER BY  idProdutoItem  desc ");
							 while($prod = mysql_fetch_array($prod_query)) 
                             {		
                                 $resultado = "S";
                            ?>
                            <li class="" style="width:100%;">
                                <div class="listing-body" style="width:100%;">
                                    <h3><a href="vermovel.php?k=<?=$prod['idProdutoItem'];?>"><?=$prod["Nome"]; ?></a></h3>
                                     <span class="list-item-info">
                                        Em <b>Móveis Planejados</b>.
                                    </span>
                                    <p>
                                      <?=substr(strip_tags( utf8_decode( $prod["Descricao"])),0,300);?>...
                                    </p>
                                   <p>
                                        <a href="vermovel.php?k=<?=$prod['idProdutoItem'];?>" class="btn btn-xs btn-two pull-right">Ver...</a>
                                    </p>
                                </div>
                            </li>
                            <?
                            }*/
                           ?>


                             <?
                             $prod_query = mysqli_query($link, "SELECT idProdutoItem, Nome, Descricao FROM NovidadeItem WHERE Nome like '%".  $_GET["p"] ."%' or Descricao like  '%".  $_GET["p"] ."%'  ORDER BY  idProdutoItem  desc ");
							 while($prod = mysqli_fetch_array($prod_query))
                             {	
                                 $resultado = "S";
                            ?>
                            <li class="" style="width:100%;">
                                <div class="listing-body" style="width:100%;">
                                    <h3><a href="vernovidade.php?k=<?=$prod['idProdutoItem'];?>"><?=$prod["Nome"]; ?></a></h3>
                                     <span class="list-item-info">
                                        Em <b>Novidade</b>.
                                    </span>
                                    <p>
                                      <?=substr(strip_tags( utf8_decode( $prod["Descricao"])),0,300);?>...
                                    </p>
                                   <p>
                                        <a href="vernovidade.php?k=<?=$prod['idProdutoItem'];?>" class="btn btn-xs btn-two pull-right">Ver...</a>
                                    </p>
                                </div>
                            </li>
                            <?
                            }
                           ?>


                        </ul>


                        <?
                        
                        if ($resultado == "") {                            echo '<div class="alert alert-warning fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Busca!</strong> Nenhum registro encontrado.</div>';                        } 
                        ?>
                  </div>
       
                </div>
                <div class="row"></div>
            </div>
        </div>
    </section>
         

    <section class="slice bg-3">

</section>  
<? require_once("rodape.php"); ?></div>

<!-- JavaScript -->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script type="text/javascript" src="js/jquery.easing.js"></script>

<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
<![endif]-->

<!-- Plugins -->
<script type="text/javascript" src="assets/hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
<script type="text/javascript" src="assets/masonry/masonry.js"></script>
<script type="text/javascript" src="assets/page-scroller/jquery.ui.totop.min.js"></script>
<script type="text/javascript" src="assets/mixitup/jquery.mixitup.js"></script>
<script type="text/javascript" src="assets/mixitup/jquery.mixitup.init.js"></script>
<script type="text/javascript" src="assets/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
<script type="text/javascript" src="assets/easy-pie-chart/jquery.easypiechart.js"></script>
<script type="text/javascript" src="assets/waypoints/waypoints.min.js"></script>
<script type="text/javascript" src="assets/sticky/jquery.sticky.js"></script>
<script type="text/javascript" src="js/jquery.wp.custom.js"></script>
<script type="text/javascript" src="js/jquery.wp.switcher.js"></script>
</body>
</html>