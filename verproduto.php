<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Revesti Decorações e Móveis Planejados</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Required -->
    <link href="css/global-style.css" rel="stylesheet" type="text/css" media="screen">
    <link rel="icon" href="images/favicon.png" type="image/png">
</head>
<body>
<? include("controle/ConnFile.php"); ?>
<? include("menu.php"); ?>
<div class="wrapper">
    <div class="pg-opt pin">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Detalhes do Produto</h2>
                </div>
                <div class="col-md-6">
                    <ol class="breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        <li class="active">Detalhes do Produto</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <?
    $prod_query = mysqli_fetch_array(mysqli_query($link, "SELECT *  FROM ProdutoItem  WHERE idProdutoItem = " . addslashes($_GET['k']) . " LIMIT 0,1 "));
    ?>
    <section class="slice bg-3">
        <div class="w-section inverse">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div id="workCarousel" class="carousel carousel-4 slide color-two-l" data-ride="carousel"
                             style="width:550px;">
                            <!-- Indicators -->
                            <ol class="carousel-indicators hide">
                                <li data-target="#homepageCarousel" data-slide-to="0" class="active"></li>
                            </ol>

                            <div class="carousel-inner" style="width:550px;">

                                <?
                                $i = 0;
                                $img_query = mysqli_query($link, "SELECT Imagem, idProdutoImagem  FROM ProdutoImagem WHERE idProduto =  " . $prod_query['idProdutoItem'] . " ORDER BY idProdutoImagem  ");
                                $active = " active";
                                while ($img = mysqli_fetch_array($img_query)) {
                                    ?>
                                    <div class="item item-dark <?= $active; ?>" id="idProdutoIMG<?= $i; ?>">
                                        <img src="gdProdutosAmp.php?imagem=<?= $img['Imagem'] ?>" alt="x" width="550"
                                             class="img-responsive">
                                    </div>
                                    <?
                                    $active = "";
                                    $i++;
                                }

                                ?>
                            </div>

                            <!-- Controls -->
                            <a class="left carousel-control" href="#workCarousel" data-slide="prev">
                                <i class="fa fa-angle-left"></i>
                            </a>
                            <a class="right carousel-control" href="#workCarousel" data-slide="next">
                                <i class="fa fa-angle-right"></i>
                            </a>

                        </div>
                        <input type="hidden" name="auxTotal" id="auxTotal" value="<?= $i; ?>">
                        <br>
                        <script>
                            function selecionarIMG(idProd) {
                                var total = parseInt($("#auxTotal").val());
                                for (var i = 0; i < total; i++) {
                                    $("#idProdutoIMG" + i.toString()).removeClass("active")
                                }
                                var nameProd = "#idProdutoIMG" + idProd;
                                $(nameProd).removeClass("active").addClass("active");
                            }
                        </script>
                        <?
                        $i = 0;
                        $img_query2 = mysqli_query($link, "SELECT Imagem, idProdutoImagem  FROM ProdutoImagem WHERE idProduto =  " . $prod_query['idProdutoItem'] . " ORDER BY idProdutoImagem  ");
                        while ($img = mysqli_fetch_array($img_query2)) {
                            ?>
                            <img src="../gdThumb.php?imagem=<?= $img['Imagem'] ?>" alt="" width="89"
                                 onclick="javascript:selecionarIMG('<?= $i; ?>');" class="img-thumbnail"
                                 style="cursor:pointer;">
                            <?
                            $i++;
                        }

                        ?>

                    </div>
                    <div class="col-md-6">
                        <div class="vertical-info">
                            <h3 class="section-title"><?= $prod_query["Nome"]; ?></h3>
                            <h4>Especificações</h4>
                            <p class="delimiter">
                                <?= $prod_query["Especificacoes"]; ?>
                            </p>

                            <h4>Marca</h4>
                            <p class="delimiter">
                                <?= $prod_query["Marca"]; ?>
                            </p>
                        </div>
                        <div style="height:20px;"></div>

                        <h4>Descrição do Produto</h4>
                        <p>
                            <?= $prod_query["Descricao"]; ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="slice relative bg-5 animate-hover-slide">
        <div class="w-section inverse">
            <div class="container">
                <h3 class="section-title">Produtos Relacionados</h3>
                <div id="carouselWork" class="carousel-3 slide animate-hover-slide">
                    <div class="carousel-nav">
                        <a data-slide="prev" class="left color-two" href="#carouselWork"><i
                                class="fa fa-angle-left"></i></a>
                        <a data-slide="next" class="right color-two" href="#carouselWork"><i
                                class="fa fa-angle-right"></i></a>
                    </div>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">


                        <?
                        $activeVar = " active ";
                        $i = 0;
                        $prod_query = mysqli_query($link, "SELECT idProdutoItem, Nome FROM ProdutoItem WHERE idProdutoCategoria = " . $prod_query['idProdutoCategoria'] . " and idProdutoItem <> " . $prod_query['idProdutoItem'] . "  ORDER BY RAND()  LIMIT 0,8");
                        while ($prod = mysqli_fetch_array($prod_query)) {
                            if ($i == 0) {
                                ?>
                                <div class="item <?= $activeVar; ?> ">
                                <div class="row">
                                <?
                                $activeVar = "";
                            }
                            $img_query = "SELECT Imagem, idProdutoImagem  FROM ProdutoImagem WHERE idProduto =  " . $prod['idProdutoItem'] . " ORDER BY idProdutoImagem DESC LIMIT 0,1 ";
                            $img = mysqli_fetch_array(mysqli_query($link, $img_query));
                            ?>
                            <div class="col-md-3" <?= $i; ?> >
                                <div class="w-box inverse">
                                    <div class="figure">
                                        <img alt="" src="gdProdutos.php?imagem=<?= $img['Imagem'] ?>"
                                             class="img-responsive">
                                        <div class="figcaption bg-2"></div>
                                        <div class="figcaption-btn">
                                            <a href="gdProdutos.php?G=S&imagem=<?= $img['Imagem'] ?>"
                                               class="btn btn-xs btn-one theater"><i class="fa fa-plus-circle"></i>
                                                Ampliar</a>
                                            <a href="verproduto.php?k=<?= $prod['idProdutoItem']; ?>"
                                               class="btn btn-xs btn-one"><i class="fa fa-link"></i> Ver</a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <h2><?= $prod["Nome"]; ?></h2>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?
                            if ($i == 3) {
                                ?>
                                </div>
                                </div>
                                <?
                            }
                            $i++;
                            if ($i == 4) {
                                $i = 0;
                            }

                        }
                        ?>

                    </div>


                </div>


            </div>
        </div>

</div>
</div>
</section>

<section class="slice bg-2 p-15">
    <div class="cta-wr">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <h1>Produtos com alta qualidade e bom gosto, para decorar e encantar!</h1>
                </div>
                <div class="col-md-4"></div>
            </div>
        </div>
    </div>
</section>

</section>   <? require_once("rodape.php"); ?></div>
<!-- JavaScript -->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script type="text/javascript" src="js/jquery.easing.js"></script>

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

<!-- Plugins -->
<script type="text/javascript" src="assets/hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
<script type="text/javascript" src="assets/masonry/masonry.js"></script>
<script type="text/javascript" src="assets/page-scroller/jquery.ui.totop.min.js"></script>
<script type="text/javascript" src="assets/mixitup/jquery.mixitup.js"></script>
<script type="text/javascript" src="assets/mixitup/jquery.mixitup.init.js"></script>
<script type="text/javascript" src="assets/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
<script type="text/javascript" src="assets/easy-pie-chart/jquery.easypiechart.js"></script>
<script type="text/javascript" src="assets/waypoints/waypoints.min.js"></script>
<script type="text/javascript" src="assets/sticky/jquery.sticky.js"></script>
<script type="text/javascript" src="js/jquery.wp.custom.js"></script>
<script type="text/javascript" src="js/jquery.wp.switcher.js"></script>
</body>
</html>