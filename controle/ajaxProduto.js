/*'Exemplo do uso do Ajax
'Autor: Andreia_Sp - http://scriptbrasil.com.br/forum/index.php?showuser=7818
'2007 versão 1.0
*/

var xmlHttp

/* Essa funo manda a ID selecionada do combo de estados para a pgina que ir filtrar as cidades */
function MandaID(str, selecionado)
{
xmlHttp=GetXmlHttpObject()
	if (xmlHttp==null)
	{
		alert ("Este browser no suporta HTTP Request")
		return
	}
	
//document.getElementById("exibe_cidade").innerHTML = "<table border='0' width='100%' cellspacing='0' cellpadding='0' height='100%'> <tr> <td align='center' valign='top'><font face='verdana' style='font-size:10px;' color='red'><img src='images/ajax-loader.gif' align='absmiddle'>&nbsp; Aguarde! Carregando.. </td> </tr> <tr> <td align='center' valign='top' height='100%'></td></tr></table>";	

document.getElementById("subcategoria").innerHTML = "<option disabled='disabled' selected='selected'>Carregando aguarde...</option>";

var url="ProdutoItemExibeSubCategoria.php" // o arquivo que ir executar a SQL das cidades
url=url+"?id="+str // recebe o ID do estado para filtrar as cidades
url=url+"&idselecionado="+selecionado;
url=url+"&sidjs="+Math.random();
xmlHttp.onreadystatechange=stateChanged
xmlHttp.open("GET",url,true)
xmlHttp.send(null)
}

/* Essa funo ir exibir o resultado na DIV */
function stateChanged()
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById("subcategoria").innerHTML=xmlHttp.responseText
	}
}

/* Instancia */
function GetXmlHttpObject()
{
var objXMLHttp=null

	if (window.XMLHttpRequest)
	{
		objXMLHttp=new XMLHttpRequest()
	}
	else if (window.ActiveXObject)
	{
		objXMLHttp=new ActiveXObject("Microsoft.XMLHTTP")
	}
return objXMLHttp
}

function ExibeIds()
{
var Estado = document.getElementById('estados');
var Cidade = document.getElementById('cidades');

if (Estado.value != '' && Cidade.value != '')
alert('ID do estado : ' + Estado.value + ' (' + Estado.options[Estado.options.selectedIndex].text + ')' + '\n\n' + 'ID da cidade : ' + Cidade.value + ' (' + Cidade.options[Cidade.options.selectedIndex].text + ')');
}