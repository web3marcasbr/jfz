<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Revesti Decorações e Móveis Planejados</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Required -->
    <link href="css/global-style.css" rel="stylesheet" type="text/css" media="screen">
    <link rel="icon" href="images/favicon.png" type="image/png">
</head>
<body>
<? include("controle/ConnFile.php"); ?>
<? include("menu.php"); ?>
<div class="wrapper">
    <!-- This section is only for demonstration purpose only. Just remove the div "divStyleSwitcher" -->
    <!-- Header: Logo and Main Nav -->

    <div class="pg-opt pin">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Móveis planejados</h2>
                </div>
                <div class="col-md-6">
                    <ol class="breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        <li class="active">Móveis planejados</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>

    <section class="slice bg-3 animate-hover-slide">
        <div class="w-section inverse blog-grid">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="widget">
                            <style>
                                .wp-theme-1 ul.categories li a:after {
                                    content: "";
                                }
                            </style>
                            <ul class="categories">

                                <?
                                $cat_query = mysqli_query($link, "SELECT idProdutoCategoria, Descricao FROM MoveisCategoria ORDER BY Descricao");
                                while ($cat = mysqli_fetch_array($cat_query)) {
                                    ?>
                                    <li>
                                        <a tabindex="-1"
                                           href="moveiscategoria.php?k=<?= $cat['idProdutoCategoria']; ?>&n=<?= $cat['Descricao']; ?>"><?= $cat["Descricao"]; ?></a>
                                    </li>
                                    <?
                                }
                                ?>
                            </ul>
                        </div>
                    </div>


                    <div class="col-md-9">
                        <!-- Content boxes -->
                        <div class="wp-example">
                            <h3 class="section-title">Diversos</h3>
                        </div>


                        <?

                        $i = 0;

                        $p = $_GET["p"];
                        if (isset($p)) {
                            $p = $p;
                        } else {
                            $p = 1;
                        }
                        $qnt = 9;
                        $inicio = ($p * $qnt) - $qnt;


                        $prod_query = mysqli_query($link, "SELECT idProdutoItem, Nome FROM MoveisItem  ORDER BY idProdutoItem DESC   LIMIT $inicio, $qnt");

                        while ($prod = mysqli_fetch_array($prod_query)) {

                            $img_query = "SELECT Imagem, idProdutoImagem  FROM MoveisImagem WHERE idProduto =  " . $prod['idProdutoItem'] . " ORDER BY idProdutoImagem DESC LIMIT 0,1 ";
                            $img = mysqli_fetch_array(mysqli_query($link, $img_query));

                            if ($i == 0) {
                                echo '<div class="row">';
                            }
                            ?>
                            <div class="col-md-4" data-cat="1" style="display: inline-block; opacity: 1;">
                                <div class="w-box inverse">
                                    <div class="figure">
                                        <img alt="" src="gdProdutos.php?imagem=<?= $img['Imagem'] ?>"
                                             class="img-responsive">
                                        <div class="figcaption bg-2"></div>
                                        <div class="figcaption-btn">
                                            <a href="gdProdutos.php?G=S&imagem=<?= $img['Imagem'] ?>"
                                               class="btn btn-xs btn-one theater"><i class="fa fa-plus-circle"></i>
                                                Ampliar</a>
                                            <a href="vermovel.php?k=<?= $prod['idProdutoItem']; ?>"
                                               class="btn btn-xs btn-one"><i class="fa fa-link"></i> Ver</a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <h2><?= $prod["Nome"]; ?></h2>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?
                            if ($i == 2) {
                                echo '</div>';
                            }
                            $i++;
                            if ($i == 3) {
                                $i = 0;
                            }

                        }

                        if ($i != 0) {
                            echo '</div>';

                        }
                        ?>

                        <center>
                            <ul class="pagination">
                                <?

                                $sql_select_all = "SELECT idProdutoItem, Nome FROM MoveisItem   ORDER BY idProdutoItem ";
                                $sql_query_all = mysqli_query($link, $sql_select_all);
                                $total_registros = mysqli_num_rows($sql_query_all);
                                $pags = ceil($total_registros / $qnt);
                                $max_links = 9;
                                echo '<li><a href="?p=1&k=' . $_GET['k'] . '&n=' . $_GET['n'] . '" target="_self">«</a></li>';
                                for ($i = $p - $max_links; $i <= $p - 1; $i++) {
                                    if ($i <= 0) {
                                    } else {
                                        echo '<li><a href="?p=' . $i . '&k=' . $_GET['k'] . '&n=' . $_GET['n'] . '" target="_self">' . $i . '</a></li> ';
                                    }
                                }
                                echo "<li class='active'><a href='#'>" . $p . "</a></li> ";
                                for ($i = $p + 1; $i <= $p + $max_links; $i++) {
                                    if ($i > $pags) {
                                    } else {
                                        echo '<li><a href="?p=' . $i . '&k=' . $_GET['k'] . '&n=' . $_GET['n'] . '" target="_self">' . $i . '</a></li> ';
                                    }
                                }

                                echo '<li><a href="?p=' . $pags . '&k=' . $_GET['k'] . '&n=' . $_GET['n'] . '" target="_self">»</a></li> ';


                                ?>
                            </ul>
                        </center>


                    </div>
                </div>
            </div>
    </section>

    </section> <? require_once("rodape.php"); ?></div>

<!-- JavaScript -->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script type="text/javascript" src="js/jquery.easing.js"></script>

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

<!-- Plugins -->
<script type="text/javascript" src="assets/hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
<script type="text/javascript" src="assets/masonry/masonry.js"></script>
<script type="text/javascript" src="assets/page-scroller/jquery.ui.totop.min.js"></script>
<script type="text/javascript" src="assets/mixitup/jquery.mixitup.js"></script>
<script type="text/javascript" src="assets/mixitup/jquery.mixitup.init.js"></script>
<script type="text/javascript" src="assets/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
<script type="text/javascript" src="assets/easy-pie-chart/jquery.easypiechart.js"></script>
<script type="text/javascript" src="assets/waypoints/waypoints.min.js"></script>
<script type="text/javascript" src="assets/sticky/jquery.sticky.js"></script>
<script type="text/javascript" src="js/jquery.wp.custom.js"></script>
<script type="text/javascript" src="js/jquery.wp.switcher.js"></script>
</body>
</html>