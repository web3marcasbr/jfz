<?php header("Content-Type: text/html; charset=UTF-8",true) ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>Revesti Decorações</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
     <!-- Required -->
    <link href="css/global-style.css" rel="stylesheet" type="text/css" media="screen">
    <link rel="icon" href="images/favicon.png" type="image/png"><!-- Fraction Slider stylesheet -->
<link rel="stylesheet" href="assets/fraction/fractionslider.css" type="text/css" />

</head>
<body>
<? include("controle/ConnFile.php"); ?>
<? include("menu.php"); ?>
<section class="slider-wrapper">
    <div class="responisve-container">
        <div class="slider">
            <div class="fs_loader"></div>
            <?php

            $slide = " slide-light";
            $banner_query = mysqli_query($link, "SELECT * FROM BannerHome WHERE Ativo = 'S' ORDER BY  idBanner DESC ");
            while($ban = mysqli_fetch_array($banner_query))
            {
            ?>
            <div class="slide <?=$slide;?>">
                <img src="img/produto/<?=$ban['Imagem1']; ?>" alt="" height="440" data-position="0,-460" data-in="bottom" data-delay="200" data-out="top">
                <img src="img/produto/<?=$ban['Imagem2']; ?>" alt="" data-position="20, 500" data-in="bottom" data-delay="400" data-out="bottom">
                
                <?
                if($ban["Frase1"] != "") {
                ?>
                <p class="claim light" data-position="70,0" data-in="top" data-step="2" data-out="top" data-delay="100">
                    <?=$ban["Frase1"]; ?>
                </p>
                <?
                }
                if($ban["Frase2"] != "") {
                ?>
                <p class="teaser dark small" data-position="130,0" data-in="bottom" data-step="2" data-delay="300">
                    <?=$ban["Frase2"]; ?>
                </p>	
                <?
                } 
                if($ban["Frase3"] != "") {
                ?>	
                <p class="teaser dark small" data-position="175,0" data-in="bottom" data-step="2" data-delay="600">
                <?=$ban["Frase3"]; ?>
                </p>
                <?
                }
                ?>

                
            </div>

            <?
                $slide = "";
            }
            ?>
    



        </div>
    </div>
</section>
    <section class="slice bg-2 p-15">
        <div class="cta-wr">
            <div class="container">
              <div class="row">
                <div class="col-md-6">
                        <h1>
                            <strong>JFZ</strong> <i class="fa fa-phone" style="font-size:32px; position:relative; bottom:-5px;"></i> (19) 3295-0800 | <i class="fa fa-whatsapp" style="font-size:32px; position:relative; bottom:-4px;"></i> (19) 9 7165-5112
                        </h1>
                  </div>
                  <div class="col-md-6">
                        <h1 style="text-align:right">
                             TUDO em até 10x SEM JUROS <br/><small style="color:inherit">nos cartões de crédito!</small>
                        </h1>
                  </div>
              </div>
            </div>
        </div>
    </section>
    <section class="slice bg-5">
      <div class="w-section inverse">
            <div class="container">
                <div class="row"></div>
            </div>
        </div>    
    </section>
    
    <section class="slice relative animate-hover-slide bg-3">
        <div class="w-section inverse">
            <div class="container">
                <h3 class="section-title">Nossos Produtos</h3>
                
                <div id="carouselWork" class="carousel-3 slide animate-hover-slide">
                    <div class="carousel-nav">
                        <a data-slide="prev" class="left" href="#carouselWork"><i class="fa fa-angle-left"></i></a>
                        <a data-slide="next" class="right" href="#carouselWork"><i class="fa fa-angle-right"></i></a>
                    </div>
                    
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="row">
							<?
							 $prod_query = mysqli_query($link, "SELECT idProdutoItem, Nome, MIN(idProdutoCategoria) FROM ProdutoItem WHERE Rotativo = 'S' GROUP BY idProdutoCategoria LIMIT 0,4");
							 while($prod = mysqli_fetch_array($prod_query))
							{			
							
							$img_query = "SELECT Imagem, idProdutoImagem  FROM ProdutoImagem WHERE idProduto =  " . $prod['idProdutoItem'] . " ORDER BY idProdutoImagem DESC LIMIT 0,1 " ;
							$img = mysqli_fetch_array(mysqli_query($link, $img_query));
							?>
                                <div class="col-md-3">
                                    <div class="w-box inverse">
                                        <div class="figure">
                                            <img alt="" src="/gdProdutos.php?imagem=<?$img['Imagem']?>" class="img-responsive">
                                            <div class="figcaption bg-2"></div>
                                            <div class="figcaption-btn">
                                                <a href="gdProdutos.php?G=S&imagem=<?$img['Imagem']?>" class="btn btn-xs btn-one theater"><i class="fa fa-plus-circle"></i> Ampliar</a>
                                                <a href="verproduto.php?k=<?$prod['idProdutoItem']; ?>" class="btn btn-xs btn-one"><i class="fa fa-link"></i> Ver</a>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                               <center><h2 style="min-height:44px;"><?=$prod["Nome"]; ?></h2></center>
                                            </div>
                                        </div>
                                    </div>
                                </div>
        					<?
							}
							?>
								</div>                             
                        </div>

                        <!-- SECOND LINE -->
                         <div class="item">
                            <div class="row">
                            <?
                             $prod_query = mysqli_query($link,"SELECT idProdutoItem, Nome, MIN(idProdutoCategoria) FROM ProdutoItem WHERE Rotativo = 'S' GROUP BY idProdutoCategoria LIMIT 4,4");
                             while($prod = mysqli_fetch_array($prod_query))
                            {           
                            
                            $img_query = "SELECT Imagem, idProdutoImagem  FROM ProdutoImagem WHERE idProduto =  " . $prod['idProdutoItem'] . " ORDER BY idProdutoImagem DESC LIMIT 0,1 " ;
                            $img = mysqli_fetch_array(mysqli_query($link, $img_query));
                            ?>
                                <div class="col-md-3">
                                    <div class="w-box inverse">
                                        <div class="figure">
                                            <img alt="" src="gdProdutos.php?imagem=<?=$img['Imagem']?>" class="img-responsive">
                                            <div class="figcaption bg-2"></div>
                                            <div class="figcaption-btn">
                                                <a href="gdProdutos.php?G=S&imagem=<?=$img['Imagem']?>" class="btn btn-xs btn-one theater"><i class="fa fa-plus-circle"></i> Ampliar</a>
                                                <a href="verproduto.php?k=<?=$prod['idProdutoItem']; ?>" class="btn btn-xs btn-one"><i class="fa fa-link"></i> Ver</a>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                               <center><h2><?=$prod["Nome"]; ?></h2></center>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?
                            }
                            ?>
                                </div>                             
                        </div>
                        <!-- THIRD LINE -->
                         <div class="item">
                            <div class="row">
                            <?
                             $prod_query = mysqli_query($link,"SELECT idProdutoItem, Nome, MIN(idProdutoCategoria) FROM ProdutoItem WHERE Rotativo = 'S' GROUP BY idProdutoCategoria LIMIT 8,4");
                             while($prod = mysqli_fetch_array($prod_query))
                            {           
                            
                            $img_query = "SELECT Imagem, idProdutoImagem  FROM ProdutoImagem WHERE idProduto =  " . $prod['idProdutoItem'] . " ORDER BY idProdutoImagem DESC LIMIT 0,1 " ;
                            $img = mysqli_fetch_array(mysqli_query($link,$img_query));
                            ?>
                                <div class="col-md-3">
                                    <div class="w-box inverse">
                                        <div class="figure">
                                            <img alt="" src="gdProdutos.php?imagem=<?=$img['Imagem']?>" class="img-responsive">
                                            <div class="figcaption bg-2"></div>
                                            <div class="figcaption-btn">
                                                <a href="gdProdutos.php?G=S&imagem=<?=$img['Imagem']?>" class="btn btn-xs btn-one theater"><i class="fa fa-plus-circle"></i> Ampliar</a>
                                                <a href="verproduto.php?k=<?=$prod['idProdutoItem']; ?>" class="btn btn-xs btn-one"><i class="fa fa-link"></i> Ver</a>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                               <center><h2><?=$prod["Nome"]; ?></h2></center>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?
                            }
                            ?>
                                </div>                             
                        </div>

                        <!-- FINNALY, THE FOURTH LINE -->
                         <div class="item">
                            <div class="row">
                            <?
                             $prod_query = mysqli_query($link, "SELECT idProdutoItem, Nome, MIN(idProdutoCategoria) FROM ProdutoItem WHERE Rotativo = 'S' GROUP BY idProdutoCategoria LIMIT 12,3");
                             while($prod = mysqli_fetch_array($prod_query))
                            {           
                            
                            $img_query = "SELECT Imagem, idProdutoImagem  FROM ProdutoImagem WHERE idProduto =  " . $prod['idProdutoItem'] . " ORDER BY idProdutoImagem DESC LIMIT 0,1 " ;
                            $img = mysqli_fetch_array(mysqli_query($link,$img_query));
                            ?>
                                <div class="col-md-3">
                                    <div class="w-box inverse">
                                        <div class="figure">
                                            <img alt="" src="gdProdutos.php?imagem=<?=$img['Imagem']?>" class="img-responsive">
                                            <div class="figcaption bg-2"></div>
                                            <div class="figcaption-btn">
                                                <a href="gdProdutos.php?G=S&imagem=<?=$img['Imagem']?>" class="btn btn-xs btn-one theater"><i class="fa fa-plus-circle"></i> Ampliar</a>
                                                <a href="verproduto.php?k=<?=$prod['idProdutoItem']; ?>" class="btn btn-xs btn-one"><i class="fa fa-link"></i> Ver</a>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                               <center><h2><?=$prod["Nome"]; ?></h2></center>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?
                            }
                            ?>
                                </div>                             
                        </div>

                       

                        




                    </div>
                </div>
                
            </div>
        </div>
    </section>
    
    <section class="slice bg-banner-1">
        <div class="w-section inverse">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center">
                            <h4><i class="fa fa-quote-left fa-3x"></i></h4>
                            <h2>Nova Coleção de Tapetes Santa Mônica</h2>
                            <p> Com tecnologia alemã, sua nova coleção apresenta resultados inéditos em termos de texturas, contrastes e <br>
                              geometrismos clássicos e contemporâneos.</p>
                            <span class="clearfix"></span>
                            
                          <div class="text-center"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="slice bg-5"></section>
    <section class="slice animate-hover-slide bg-5">
        <div class="w-section inverse">
            <div class="container">
                <h3 class="section-title">Novidades</h3>
                <div class="row">
                    
					<?
							 $prod_query = mysqli_query($link,"SELECT idProdutoItem, Nome, Descricao, Data FROM NovidadeItem WHERE Rotativo = 'S' ORDER BY  idProdutoItem DESC LIMIT 0,4");
							 while($prod = mysqli_fetch_array($prod_query))
							{			
							
							$img_query = "SELECT Imagem, idProdutoImagem  FROM NovidadeImagem WHERE idProduto =  " . $prod['idProdutoItem'] . " ORDER BY idProdutoImagem DESC LIMIT 0,1 " ;
							$img = mysqli_fetch_array(mysqli_query($link,$img_query));
							?>
							
					<div class="col-md-3">
                        <div class="w-box">
                            <div class="figure">
                                <img alt="" src="gdProdutos.php?imagem=<?=$img['Imagem']?>" class="img-responsive">
                                <span class="date-over small"><strong><?=date("d/m/Y", strtotime($prod["Data"]));?></strong></span>
                                <div class="figcaption bg-2"></div>
                                <div class="figcaption-btn">
                                    <a href="gdProdutos.php?G=S&imagem=<?=$img['Imagem']?>" class="btn btn-xs btn-one theater"><i class="fa fa-plus-circle"></i> Ampliar</a>
                                    <a href="vernovidade.php?k=<?=$prod['idProdutoItem']; ?>" class="btn btn-xs btn-one"><i class="fa fa-link"></i> Ver</a>
                                </div>
                            </div>
                          <h2><?=$prod["Nome"]?></h2>
                            <p>
								<?=substr(strip_tags($prod["Descricao"]),0,72);?>...
                            </p>
                      </div>
                    </div>
					
					<? 
					}?>
                    
                    
              
                    </div>
        
                </div>
            </div>
      </div>
</section>
  <section class="slice bg-2 p-15">
        <div class="cta-wr">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <h1>Produtos com alta qualidade e bom gosto, para decorar e encantar!</h1>
                  </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="slice bg-3">
  <div class="w-section inverse">
        <div class="container">
        	<h3 class="section-title">Marcas</h3>
            <div class="row">
			
<? require_once("marca.php");?>
            
            </div>
        </div>
    </div>
</section>    


<? require_once("rodape.php"); ?>


</div>
<!-- JavaScript -->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script type="text/javascript" src="js/jquery.easing.js"></script>

<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
<![endif]-->

<!-- Plugins -->
<script type="text/javascript" src="assets/hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
<script type="text/javascript" src="assets/masonry/masonry.js"></script>
<script type="text/javascript" src="assets/page-scroller/jquery.ui.totop.min.js"></script>
<script type="text/javascript" src="assets/mixitup/jquery.mixitup.js"></script>
<script type="text/javascript" src="assets/mixitup/jquery.mixitup.init.js"></script>
<script type="text/javascript" src="assets/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
<script type="text/javascript" src="assets/easy-pie-chart/jquery.easypiechart.js"></script>
<script type="text/javascript" src="assets/waypoints/waypoints.min.js"></script>
<script type="text/javascript" src="assets/sticky/jquery.sticky.js"></script>
<script type="text/javascript" src="js/jquery.wp.custom.js"></script>
<script type="text/javascript" src="js/jquery.wp.switcher.js"></script>
<script src="assets/fraction/jquery.fractionslider.min.js"></script>
<script src="assets/fraction/jquery.fractionslider.init.js"></script>
 
<!-- LayerSlider script files -->
<script src="assets/layerslider/js/layerslider.transitions.js" type="text/javascript"></script>
<script src="assets/layerslider/js/layerslider.kreaturamedia.jquery.js" type="text/javascript"></script>
<!-- Initializing the slider -->
	<script>
		jQuery("#layerslider").layerSlider({
			pauseOnHover: true,
			autoPlayVideos: false,
			skinsPath: 'assets/layerslider/skins/',
			responsive: false,
			responsiveUnder: 1280,
			layersContainer: 1280,
			skin: 'v5',
			hoverPrevNext: false,
		});
	</script>
</body>
</html>