<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>Revesti Decorações e Móveis Planejados</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
     <!-- Required -->
    <link href="css/global-style.css" rel="stylesheet" type="text/css" media="screen">
    <link rel="icon" href="images/favicon.png" type="image/png">
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
<script type="text/javascript">
function initialize() {
    var myLatlng = new google.maps.LatLng(-22.895354, -47.049483);
  var mapOptions = {
    zoom: 16,
	scrollwheel: false,
    center: myLatlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }
  var map = new google.maps.Map(document.getElementById('mapCanvas'), mapOptions);

  var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
	  animation: google.maps.Animation.DROP,
	  title: 'Revesti Decorações!' 
  });
  
  var contentString = '<div class="info-window-content"><h2>Revesti Decorações e Móveis Planejados</h2>' +
					  '<p>Rua Antônio Lapa, 606 - Cambuí - Campinas/SP<br>Fone: (19) 3295-0800</p></div>';
					  
  var infowindow = new google.maps.InfoWindow({
      content: contentString
  });
  
  google.maps.event.addListener(marker, 'click', function() {
    infowindow.open(map,marker);
  });
}

google.maps.event.addDomListener(window, 'load', initialize);

</script>

</head>
<body>
<? include("controle/ConnFile.php"); ?>
<? include("menu.php"); ?>

<div class="wrapper">
	<!-- This section is only for demonstration purpose only. Just remove the div "divStyleSwitcher" -->
  <!-- Header: Logo and Main Nav -->

    <div class="pg-opt pin">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Contato</h2>
                </div>
                <div class="col-md-6">
                    <ol class="breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        <li>Contato</li>
                    </ol>
                </div>
                
            </div>
        </div>
    </div>
    
    <section class="slice bg-3">
        <div class="w-section inverse">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <h3 class="section-title">Envie uma mensagem</h3>
                        <p>
                        Entre em contato pelo formulário abaixo, estaremos a disposição parar sanar suas dúvidas, receber suas critícas, elogios e aceitar sugestões!<br>
                        Se preferir, entre em contato pelo e-mail: contato@revestidecor.com.br</p>
                        
                        <form class="form-light mt-20" method="post" action="?enviar=s" role="form">
                          <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control" name="nome" required placeholder="Seu Nome">
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" class="form-control" required  name="email" placeholder="Endereço de E-mail">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Fone</label><input type="text" class="form-control"  name="telefone" required placeholder="Número do Telefone">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Assunto</label><input type="text" class="form-control" id="subject" required  name="assunto"  placeholder="Qual o Assunto?">
                            </div>
                            <div class="form-group">
                                <label>Messagem</label>
                              <textarea class="form-control" placeholder="Escreva sua mensagem..." name="mensagem"  required style="height:100px;"></textarea>
                            </div>
                            <button type="submit" class="btn btn-two">Enviar </button>
                            <?
                            if($_GET["enviar"] == "s")
                            {
                                $nome = $_POST["nome"];
                                $email = $_POST["email"];
                                $fone = $_POST["telefone"];
                                $assunto = $_POST["assunto"];
                                $mensagem  = $_POST["mensagem"];
                                
                                
                           
                            require_once("class/class.phpmailer.php");                            $mail = new PHPMailer(); 
                            $mail->SetLanguage("br"); 
                            $mail->CharSet = "iso-8859-1"; 
                            $mail->IsSMTP(); 
                            $mail->Host = "mail.revestidecor.com.br"; 
                            $mail->SMTPAuth = true;
                            $mail->Username = "noreplay@revestidecor.com.br"; 
                            $mail->Password = "n0r3ply"; 
                            $mail->IsHTML(true);
                            $mail->From = "noreplay@revestidecor.com.br"; 
                            $mail->FromName = "Revesti Decor"; 
                            $mail->AddAddress("contato@revestidecor.com.br","Revesti Decor");


                            $mail->AddReplyTo("noreplay@revestidecor.com.br","Contato site");

                            $mail->AltBody = ""; 


                                $mail->Subject  = "Contato enviado pelo site Revisti Decor"; 
                            $body = "<table width='100%' height='219' border='0' cellpadding='10' cellspacing='0'>                                            <tr>
                                            <td height='42' colspan='2' align='center' valign='middle' bgcolor='#000000'><font face='Verdana, Geneva, sans-serif' color='#FFFFFF' size='+1'>Contato feito pelo site!</font></td>
                                            </tr>
                                            <tr>
                                            <td width='88' height='25' valign='top'><font face='Verdana, Geneva, sans-serif' size='-1' color='#333333'><strong>Nome</strong></font><strong>:</strong></td>
                                            <td width='1009' valign='top'><font face='Verdana, Geneva, sans-serif' size='-1' color='#333333'>$nome</font></td>
                                            </tr>
                                            <tr>
                                            <td width='88' height='25' valign='top'><font face='Verdana, Geneva, sans-serif' size='-1' color='#333333'><strong>Assunto</strong></font><strong>:</strong></td>
                                            <td width='1009' valign='top'><font face='Verdana, Geneva, sans-serif' size='-1' color='#333333'>$assunto</font></td>
                                            </tr>
                                            <tr>
                                            <td height='25' valign='top'><font face='Verdana, Geneva, sans-serif' size='-1' color='#333333'><strong>E-mail:</strong></font></td>
                                            <td valign='top'><font face='Verdana, Geneva, sans-serif' size='-1' color='#333333'>$email</font></td>
                                            </tr>
                                            <tr>
                                            <td height='25' valign='top'><font face='Verdana, Geneva, sans-serif' size='-1' color='#333333'><strong>Telefone:</strong></font></td>
                                            <td valign='top'><font face='Verdana, Geneva, sans-serif' size='-1' color='#333333'>$fone</font></td>
                                            </tr>
                                            <tr>
                                            <td height='25' valign='top'><font face='Verdana, Geneva, sans-serif' size='-1' color='#333333'><strong>Mensagem:</strong></font></td>
                                            <td valign='top'><font face='Verdana, Geneva, sans-serif' size='-1' color='#333333'>$mensagem</font></td>
                                            </tr>
                                        </table>";
                                
           
                                
                            $mail->Body =  $body;

                                $enviado = $mail->Send();                              
                                //// Limpa os destinatários e os anexos                                //$mail->ClearAllRecipients();                                //$mail->ClearAttachments();
                                // Exibe uma mensagem de resultado
                                if ($enviado) {
                                    //echo "E-mail enviado com sucesso!";
                                    echo '<div class="alert alert-success fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Contato!</strong> E-mail enviado com sucesso.</div>';
                                
								?>
<!-- Google Code for Form_Contato Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 988396516;
var google_conversion_language = "en";
var google_conversion_format = "2";
var google_conversion_color = "ffffff";
var google_conversion_label = "qw12CMW4i1kQ5Pem1wM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript"
src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/988396516/?label=qw12CMW4i1kQ5Pem1wM&amp;guid=ON&amp;script=0" />
</div>
</noscript>
<?
								} else {
                                    
                                    echo '<div class="alert alert-danger fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Contato!</strong> Não foi possível enviar o e-mail. '. $mail->ErrorInfo .' </div>';
                                    
                                }
                                
                                
                            
                            }
                            ?>
                        </form>
                    </div>
                    
                    <div class="col-md-5">
                        <div id="mapCanvas" class="map-canvas no-margin"></div>
                        
                      <div class="row">
                        <div class="col-sm-6">
                                <div class="subsection">
                                    <h3 class="section-title">Entre em Contato</h3>
                                    <div class="contact-info">
                                        <h5>Endereço</h5>
                                        <p>Rua Antônio Lapa, 606 <br/> Cambuí - Campinas/SP
</p>
                                        
                                        <h5>E-mail</h5>
                                        <p>contato@revestidecor.com.br</p>
                                        
                                        <h5>Fone</h5>
                                        <p>(19) 3295-0800 </p>
										<p>(19) 9 7165-5112 </p>
                                    </div>
                                </div>
                            </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    
</section>  <? require_once("rodape.php"); ?></div>

<!-- JavaScript -->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script type="text/javascript" src="js/jquery.easing.js"></script>

<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
<![endif]-->

<!-- Plugins -->
<script type="text/javascript" src="assets/hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
<script type="text/javascript" src="assets/masonry/masonry.js"></script>
<script type="text/javascript" src="assets/page-scroller/jquery.ui.totop.min.js"></script>
<script type="text/javascript" src="assets/mixitup/jquery.mixitup.js"></script>
<script type="text/javascript" src="assets/mixitup/jquery.mixitup.init.js"></script>
<script type="text/javascript" src="assets/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
<script type="text/javascript" src="assets/easy-pie-chart/jquery.easypiechart.js"></script>
<script type="text/javascript" src="assets/waypoints/waypoints.min.js"></script>
<script type="text/javascript" src="assets/sticky/jquery.sticky.js"></script>
<script type="text/javascript" src="js/jquery.wp.custom.js"></script>
<script type="text/javascript" src="js/jquery.wp.switcher.js"></script>
</body>
</html>