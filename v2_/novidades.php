<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>Revesti Decorações e Móveis Planejados</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
     <!-- Required -->
    <link href="css/global-style.css" rel="stylesheet" type="text/css" media="screen">
    <link rel="icon" href="images/favicon.png" type="image/png"><!-- LayerSlider stylesheet -->
<link rel="stylesheet" href="assets/layerslider/css/layerslider.css" type="text/css">

</head>
<body>

<? include("controle/ConnFile.php"); ?>
<? include("menu.php"); ?>
<div class="wrapper">
	<!-- This section is only for demonstration purpose only. Just remove the div "divStyleSwitcher" -->
  <!-- Header: Logo and Main Nav -->
 
	<div class="pg-opt pin">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Novidades</h2>
                </div>
                <div class="col-md-6">
                    <ol class="breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        <li class="active">Novidades</li>
                    </ol>
                </div>
                
            </div>
        </div>
    </div>
    
    <section class="slice bg-2">
        <div class="w-section inverse">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center">
                            <h4><i class="fa fa-comment fa-3x"></i></h4>
                            <h2>Fique por dentro das novidades!</h2>
                            <p>
                                Aqui você encontrará os últimos lançamentos do mundo da decoração.
                            </p>
                            <span class="clearfix"></span>
    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="slice bg-3">
        <div class="w-section inverse">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <ul class="list-listings blog-list">
                            <?
							$p = $_GET["p"]; 
							if(isset($p)) { $p = $p; } else { $p = 1; }
							$qnt = 5;
							$inicio = ($p*$qnt) - $qnt;

							
                            $prod_query = mysql_query("SELECT idProdutoItem, Nome, Descricao, Data FROM NovidadeItem ORDER BY idProdutoItem DESC  LIMIT $inicio, $qnt ");
							 while($prod = mysql_fetch_array($prod_query)) 
							{			
							
                                $img_query = "SELECT Imagem, idProdutoImagem  FROM NovidadeImagem WHERE idProduto =  " . $prod['idProdutoItem'] . " ORDER BY idProdutoImagem DESC LIMIT 0,1 " ;
							    $img = mysql_fetch_array(mysql_query($img_query));
							?>
							<li class="">
                                <div class="listing-image">
                                   <img alt="" src="gdProdutos.php?imagem=<?=$img['Imagem']?>" class="img-responsive">
                                </div>
                                <div class="listing-body">
                                    <h3><a href="#"><?=$prod["Nome"]; ?></a></h3>
                                    <span class="list-item-info">
                                        Postado em <?=date("d/m/Y", strtotime($prod["Data"]));?>
                                    </span>
                                    <p>
                                        <?=substr(strip_tags( utf8_decode( $prod["Descricao"])),0,300);?>...
                                    </p>
                                   <p>
                                        <a href="vernovidade.php?k=<?=$prod['idProdutoItem'];?>" class="btn btn-xs btn-two pull-right">Saiba Mais...</a>
                                    </p>
                                </div>
                            </li>
						<?
						}	
						 ?>
						</ul>
                        <ul class="pagination">
						<?
						
						$sql_select_all = "SELECT idProdutoItem, Nome FROM NovidadeItem   ORDER BY idProdutoItem "; 
						$sql_query_all = mysql_query($sql_select_all); 
						$total_registros = mysql_num_rows($sql_query_all); 
						$pags = ceil($total_registros/$qnt); 
						$max_links = 9; 
						echo '<li><a href="?p=1&k='. $_GET['k'] .'&n='. $_GET['n'] .'" target="_self">«</a></li>'; 
						for($i = $p-$max_links; $i <= $p-1; $i++) 
						{ 
							if($i <=0) { 
							} else {
							echo '<li><a href="?p='.$i.'&k='. $_GET['k'] .'&n='. $_GET['n'] .'" target="_self">'.$i.'</a></li> '; 
							} 
						} 
						echo  "<li class='active'><a href='#'>". $p."</a></li> "; 
						for($i = $p+1; $i <= $p+$max_links; $i++) { 
						if($i > $pags) { 
						} 
						else { 
						echo '<li><a href="?p='.$i.'&k='. $_GET['k'] .'&n='. $_GET['n'] .'" target="_self">'.$i.'</a></li> '; } 
						} 

						echo '<li><a href="?p='.$pags.'&k='. $_GET['k'] .'&n='. $_GET['n'] .'" target="_self">»</a></li> ';


						
						?>
						</ul>
                    </div>
                    <div class="col-md-4">
                        <h3 class="section-title">Encontre o que procura</h3>
                        <div class="widget">
                            <form class="form-inline" action="busca.php" method="get">
                                <div class="input-group">
                                    <script>

                                                    function Pesquisar2() {
                                                        location.href = "busca.php?p=" + document.getElementById("p2").value;
                                                    }
                                                </script>
                                    <input type="text" id="p2" name="p" class="form-control" placeholder="Busque o que deseja..." />
                                    <span class="input-group-btn">
                                        <button class="btn btn-two" type="button" onclick="Pesquisar2();">OK!</button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="slice bg-2 p-15">
        <div class="cta-wr">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <h1>Produtos com alta qualidade e bom gosto, para decorar e encantar!</h1>
                  </div>
                    <div class="col-md-4"></div>
                </div>
            </div>
        </div>
</section>    
 <? require_once("rodape.php"); ?></div>
<!-- JavaScript -->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script type="text/javascript" src="js/jquery.easing.js"></script>

<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
<![endif]-->

<!-- Plugins -->
<script type="text/javascript" src="assets/hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
<script type="text/javascript" src="assets/masonry/masonry.js"></script>
<script type="text/javascript" src="assets/page-scroller/jquery.ui.totop.min.js"></script>
<script type="text/javascript" src="assets/mixitup/jquery.mixitup.js"></script>
<script type="text/javascript" src="assets/mixitup/jquery.mixitup.init.js"></script>
<script type="text/javascript" src="assets/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
<script type="text/javascript" src="assets/easy-pie-chart/jquery.easypiechart.js"></script>
<script type="text/javascript" src="assets/waypoints/waypoints.min.js"></script>
<script type="text/javascript" src="assets/sticky/jquery.sticky.js"></script>
<script type="text/javascript" src="js/jquery.wp.custom.js"></script>
<script type="text/javascript" src="js/jquery.wp.switcher.js"></script>
</body>
</html>