<nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Revesti Decor</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Produto <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="ProdutoCategoria.php">Categoria</a></li>
			<li><a href="ProdutoSubCategoria.php">Sub-Categoria</a></li>
			<li><a href="ProdutoItem.php">Item</a></li>
          </ul>
        </li>
		
		<li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Móveis Planejados <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="MoveisCategoria.php">Categoria</a></li>
            <li><a href="MoveisItem.php">Item</a></li>
          </ul>
        </li>
		
		<li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Banner <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="BannerHomeItem.php">Home</a></li>
            <li><a href="BannerQuemSomos.php">Quem somos</a></li>
          </ul>
        </li>
		<li><a href="NovidadeItem.php">Novidade</a></li>
        <li><a href="Marca.php">Marca</a></li>
		<li><a href="excel.php">Newsletter</a></li>	
      </ul>
      </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<div style="width:98%; padding-left:1%;">
<div class="panel panel-default">
  <div class="panel-body">