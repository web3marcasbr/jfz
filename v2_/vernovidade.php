<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>Revesti Decorações e Móveis Planejados</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
     <!-- Required -->
    <link href="css/global-style.css" rel="stylesheet" type="text/css" media="screen">
    <link rel="icon" href="images/favicon.png" type="image/png">
</head>
<body>
<? include("controle/ConnFile.php"); ?>
<? include("menu.php"); ?>
<div class="wrapper">
    <div class="pg-opt pin">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Novidades</h2>
                </div>
                <div class="col-md-6">
                    <ol class="breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        <li class="active">Novidades</li>
                    </ol>
                </div>
                
            </div>
        </div>
    </div>
    <?
	 $prod_query = mysql_fetch_array(mysql_query("SELECT *  FROM NovidadeItem  WHERE idProdutoItem = ". addslashes($_GET['k']). " LIMIT 0,1 "));
	
	?>
    <section class="slice bg-3">
        <div class="w-section inverse">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div id="workCarousel" class="carousel carousel-4 slide color-two-l" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators hide">
                                <li data-target="#homepageCarousel" data-slide-to="0" class="active"></li>
                            </ol>
                            
                            <div class="carousel-inner">
							
							<?
							
							$img_query = mysql_query("SELECT Imagem, idProdutoImagem  FROM NovidadeImagem WHERE idProduto =  " . $prod_query['idProdutoItem'] . " ORDER BY idProdutoImagem  ") ;
							$active = " active";
							while($img = mysql_fetch_array($img_query)) {;
							?>
                                <div class="item item-dark <?=$active;?>">
                                    <img src="gdProdutosAmp.php?imagem=<?=$img['Imagem']?>" alt="" class="img-responsive">
                                </div>
                               <?
							   $active = "";
							   }
							   
							   ?>
                            </div>
                            
                            <!-- Controls -->
                            <a class="left carousel-control" href="#workCarousel" data-slide="prev">
                                <i class="fa fa-angle-left"></i>
                            </a>
                            <a class="right carousel-control" href="#workCarousel" data-slide="next">
                                <i class="fa fa-angle-right"></i>
                            </a>     
                            
                        </div>
                    </div>
                    <div class="col-md-6">
                      <div class="vertical-info">                      
                        <h4 class="section-title">  <?=$prod_query["Nome"]; ?></h4>
                        <p>
                          <?=$prod_query["Descricao"]; ?>
						</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="slice relative bg-5 animate-hover-slide">
        <div class="w-section inverse">
            <div class="container">
          
                
						 
						 
						 
                            </div>
                        
                        
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    
    <section class="slice bg-2 p-15">
        <div class="cta-wr">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <h1>Produtos com alta qualidade e bom gosto, para decorar e encantar!</h1>
                    </div>
                    <div class="col-md-4"></div>
                </div>
            </div>
        </div>
    </section>
    
</section>   <? require_once("rodape.php"); ?></div>
<!-- JavaScript -->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script type="text/javascript" src="js/jquery.easing.js"></script>

<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
<![endif]-->

<!-- Plugins -->
<script type="text/javascript" src="assets/hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
<script type="text/javascript" src="assets/masonry/masonry.js"></script>
<script type="text/javascript" src="assets/page-scroller/jquery.ui.totop.min.js"></script>
<script type="text/javascript" src="assets/mixitup/jquery.mixitup.js"></script>
<script type="text/javascript" src="assets/mixitup/jquery.mixitup.init.js"></script>
<script type="text/javascript" src="assets/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
<script type="text/javascript" src="assets/easy-pie-chart/jquery.easypiechart.js"></script>
<script type="text/javascript" src="assets/waypoints/waypoints.min.js"></script>
<script type="text/javascript" src="assets/sticky/jquery.sticky.js"></script>
<script type="text/javascript" src="js/jquery.wp.custom.js"></script>
<script type="text/javascript" src="js/jquery.wp.switcher.js"></script>
</body>
</html>