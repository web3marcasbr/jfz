<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>Revesti Decorações e Móveis Planejados</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
     <!-- Required -->
    <link href="css/global-style.css" rel="stylesheet" type="text/css" media="screen">
    <link rel="icon" href="images/favicon.png" type="image/png">
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
<script type="text/javascript">
function initialize() {
    var myLatlng = new google.maps.LatLng(-22.9048208, -47.0410348);
  var mapOptions = {
    zoom: 16,
	scrollwheel: false,
    center: myLatlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }
  var map = new google.maps.Map(document.getElementById('mapCanvas'), mapOptions);

  var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
	  animation: google.maps.Animation.DROP,
	  title: 'Revesti Decorações!' 
  });
  
  var contentString = '<div class="info-window-content"><h2>Revesti Decorações e Móveis Planejados</h2>' +
					  '<p>Avenida Jesuíno Marcondes Machado, 189<br> Nova Campinas - Campinas/SP<br>Fone: (19) 3295-0800</p></div>';
					  
  var infowindow = new google.maps.InfoWindow({
      content: contentString
  });
  
  google.maps.event.addListener(marker, 'click', function() {
    infowindow.open(map,marker);
  });
}

google.maps.event.addDomListener(window, 'load', initialize);

</script>

</head>
<body>
<? include("controle/ConnFile.php"); ?>
<? include("menu.php"); ?>

<div class="wrapper">
	<!-- This section is only for demonstration purpose only. Just remove the div "divStyleSwitcher" -->
  <!-- Header: Logo and Main Nav -->

    <div class="pg-opt pin">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Contato</h2>
                </div>
                <div class="col-md-6">
                    <ol class="breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        <li>Contato</li>
                    </ol>
                </div>
                
            </div>
        </div>
    </div>
    
    <section class="slice bg-3">
        <div class="w-section inverse">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <h3 class="section-title">Envie uma mensagem</h3>
                        <p>
                        Entre em contato pelo formulário abaixo, estaremos a disposição parar sanar suas dúvidas, receber suas critícas, elogios e aceitar sugestões!<br>
                        Se preferir, entre em contato pelo e-mail: contato@revestidecor.com.br</p>
                        
                        <form class="form-light mt-20" method="post" action="?enviar=s" role="form">
                          <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control" name="nome" required placeholder="Seu Nome">
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" class="form-control" required  name="email" placeholder="Endereço de E-mail">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Fone</label><input type="text" class="form-control"  name="telefone" required placeholder="Número do Telefone">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Assunto</label><input type="text" class="form-control" id="subject" required  name="assunto"  placeholder="Qual o Assunto?">
                            </div>
                            <div class="form-group">
                                <label>Messagem</label>
                              <textarea class="form-control" placeholder="Escreva sua mensagem..." name="mensagem"  required style="height:100px;"></textarea>
                            </div>
                            <button type="submit" class="btn btn-two">Enviar </button>
                            <?
                            if($_GET["enviar"] == "s")
                            {
                                $nome = $_POST["nome"];
                                $email = $_POST["email"];
                                $fone = $_POST["telefone"];
                                $assunto = $_POST["assunto"];
                                $mensagem  = $_POST["mensagem"];
                                
                                
                                // Inclui o arquivo class.phpmailer.php localizado na pasta phpmailer
                                require("class/class.phpmailer.php");
                                
                                // Inicia a classe PHPMailer                                $mail = new PHPMailer();
                              
                                // Define o remetente                                // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=                                $mail->From = "noreplay@revestidecor.com.br"; // Seu e-mail
                                $mail->Sender = "noreplay@revestidecor.com.br"; // Seu e-mail
                                $mail->FromName = "Revesti Decor"; // Seu nome
                                
                                // Define os destinatário(s)                                // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                                $destinatario = utf8_decode("Singularis");
                                $mail->AddAddress("contato@revestidecor.com.br", "contato@revestidecor.com.br");
                                                           
                                // Define os dados técnicos da Mensagem                                // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                                $mail->IsHTML(true); // Define que o e-mail será enviado como HTML
                                //$mail->CharSet = "iso-8859-1"; // Charset da mensagem (opcional)                                // Define a mensagem (Texto e Assunto)                                // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=                                $mail->Subject  = "Contato enviado pelo site Revisti Decor"; // Assunto da mensagem
                                $mail->Body = "<table width='100%' height='219' border='0' cellpadding='10' cellspacing='0'>                                                <tr>
                                                <td height='42' colspan='2' align='center' valign='middle' bgcolor='#000000'><font face='Verdana, Geneva, sans-serif' color='#FFFFFF' size='+1'>Contato feito pelo site!</font></td>
                                                </tr>
                                                <tr>
                                                <td width='88' height='25' valign='top'><font face='Verdana, Geneva, sans-serif' size='-1' color='#333333'><strong>Nome</strong></font><strong>:</strong></td>
                                                <td width='1009' valign='top'><font face='Verdana, Geneva, sans-serif' size='-1' color='#333333'>$nome</font></td>
                                                </tr>
                                                <tr>
                                                <td width='88' height='25' valign='top'><font face='Verdana, Geneva, sans-serif' size='-1' color='#333333'><strong>Assunto</strong></font><strong>:</strong></td>
                                                <td width='1009' valign='top'><font face='Verdana, Geneva, sans-serif' size='-1' color='#333333'>$assunto</font></td>
                                                </tr>
                                                <tr>
                                                <td height='25' valign='top'><font face='Verdana, Geneva, sans-serif' size='-1' color='#333333'><strong>E-mail:</strong></font></td>
                                                <td valign='top'><font face='Verdana, Geneva, sans-serif' size='-1' color='#333333'>$email</font></td>
                                                </tr>
                                                <tr>
                                                <td height='25' valign='top'><font face='Verdana, Geneva, sans-serif' size='-1' color='#333333'><strong>Telefone:</strong></font></td>
                                                <td valign='top'><font face='Verdana, Geneva, sans-serif' size='-1' color='#333333'>$fone</font></td>
                                                </tr>
                                                <tr>
                                                <td height='25' valign='top'><font face='Verdana, Geneva, sans-serif' size='-1' color='#333333'><strong>Mensagem:</strong></font></td>
                                                <td valign='top'><font face='Verdana, Geneva, sans-serif' size='-1' color='#333333'>$mensagem</font></td>
                                                </tr>
                                            </table>";

                                $mail->AltBody = "$mensagem \r\n ";
                                                              
                                // Envia o e-mail
                                $enviado = $mail->Send();                              
                                // Limpa os destinatários e os anexos                                $mail->ClearAllRecipients();                                $mail->ClearAttachments();
                                // Exibe uma mensagem de resultado
                                if ($enviado) {
                                    //echo "E-mail enviado com sucesso!";
                                    echo '<div class="alert alert-success fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Contato!</strong> E-mail enviado com sucesso.</div>';
                                } else {
                                    
                                    echo '<div class="alert alert-danger fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Contato!</strong> E-mail enviado com sucesso.</div>';
                                    
                                }
                                
                                
                            
                            }
                            ?>
                        </form>
                    </div>
                    
                    <div class="col-md-5">
                        <div id="mapCanvas" class="map-canvas no-margin"></div>
                        
                      <div class="row">
                        <div class="col-sm-6">
                                <div class="subsection">
                                    <h3 class="section-title">Entre em Contato</h3>
                                    <div class="contact-info">
                                        <h5>Endereço</h5>
                                        <p>Avenida Jesuíno Marcondes Machado, 189
</p>
                                        
                                        <h5>E-mail</h5>
                                        <p>contato@revestidecor.com.br</p>
                                        
                                        <h5>Fone</h5>
                                        <p>(19) 3295-0800 </p>
                                    </div>
                                </div>
                            </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    
</section>  <? require_once("rodape.php"); ?></div>

<!-- JavaScript -->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script type="text/javascript" src="js/jquery.easing.js"></script>

<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
<![endif]-->

<!-- Plugins -->
<script type="text/javascript" src="assets/hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
<script type="text/javascript" src="assets/masonry/masonry.js"></script>
<script type="text/javascript" src="assets/page-scroller/jquery.ui.totop.min.js"></script>
<script type="text/javascript" src="assets/mixitup/jquery.mixitup.js"></script>
<script type="text/javascript" src="assets/mixitup/jquery.mixitup.init.js"></script>
<script type="text/javascript" src="assets/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
<script type="text/javascript" src="assets/easy-pie-chart/jquery.easypiechart.js"></script>
<script type="text/javascript" src="assets/waypoints/waypoints.min.js"></script>
<script type="text/javascript" src="assets/sticky/jquery.sticky.js"></script>
<script type="text/javascript" src="js/jquery.wp.custom.js"></script>
<script type="text/javascript" src="js/jquery.wp.switcher.js"></script>
</body>
</html>