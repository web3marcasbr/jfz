
<section id="asideMenu" class="aside-menu">
	<form class="form-inline form-search">
        <div class="input-group">
            <input type="search" class="form-control" placeholder="Pesquisar..." />
            <span class="input-group-btn">
                <button id="btnHideAsideMenu" class="btn btn-close" type="button" title="Hide sidebar"><i class="fa fa-times"></i></button>
            </span>
        </div>
    </form>
	
    <h5 class="side-section-title">Menu</h5>
    <div class="nav">
    	<ul>
        	<li>
            	<a href="index.php">Home</a>
            </li>
            <li>
            	<a href="quemsomos.php">Empresa</a>
            </li>
            <li>
            	<a href="produtos.php">Produtos</a>
            </li>
            <li>
            	<a href="moveis.php"><?=utf8_encode("M�veis"); ?> Planejados</a>
            </li>
            <li>
            	<a href="novidades.php">Novidades</a>
            </li>
            <li>
            	<a href="contato.php">Contato</a>
            </li>
        </ul>
    </div>
    
    <h5 class="side-section-title">Redes Sociais</h5>
    <div class="social-media">
        <a href="https://www.facebook.com/RevestiDecor"><i class="fa fa-facebook facebook"></i></a>
        <a href="http://www.instagram.com/revestidecor"><i class="fa fa-Instagram"></i></a>
    </div>
    
    <h5 class="side-section-title">Informa��es para Contato</h5>
    <div class="contact-info">
        <h5>Endere�o</h5>
        <p>Avenida Jesu�no Marcondes Machado, 189 - Nova Campinas - Campinas/SP</p>
        
        <h5>E-mail</h5>
        <p>contato@revestidecor.com.br</p>
        
        <h5>Fone</h5>
        <p>(19) 3295-0800</p>
    </div>
</section>
<div class="wrapper">
	<!-- This section is only for demonstration purpose only. Just remove the div "divStyleSwitcher" --><!-- Top Header -->
<!-- Header: Logo and Main Nav -->
<header>    
	<div id="navTwo" class="navbar navbar-wp" role="navigation">
        <div class="container">
            <div class="navbar-header" style="height:100px;">
            	<button type="button" class="navbar-toggle navbar-toggle-aside-menu">
                    <i class="fa fa-outdent icon-custom"></i>
                </button>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Alternar Navega��o</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php" title="Revesti Decora��es e M�veis Planejados" style="height:70px;">
                	<img src="images/boomerang-logo.png" style="height:70px;" alt="Revesti Decora��es e M�veis Planejados">
                </a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                	<li class="active">
                    	<a href="index.php" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">Home</a>
                	</li>
                    <li class="dropdown">
                    	<a href="quemsomos.php" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">Quem Somos</a>
                	</li>
                    <li class="dropdown">
                    	<a href="produtos.php" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">Produtos</a>
                        <ul class="dropdown-menu dropdown-megamenu">
                        	 <?
							 $prod_query = mysql_query("SELECT idProdutoCategoria, Descricao FROM ProdutoCategoria ORDER BY Descricao");
							 while($prod = mysql_fetch_array($prod_query)) 
							{			
							
							
							?>
							<li class="dropdown-submenu">
                                <a tabindex="-1" href="categoria.php?k=<?=$prod['idProdutoCategoria']; ?>&n=<?=$prod['Descricao'];?>"><?=$prod["Descricao"];?></a>
								<?
								$sub_query = mysql_query("SELECT Descricao, idProdutoSubCategoria , IdProdutoCategoria FROM ProdutoSubCategoria WHERE IdProdutoCategoria =  " . $prod['idProdutoCategoria'] . " ORDER BY Descricao  ");
								
								if(mysql_num_rows($sub_query) > 0 ) {
								
								?>
                                <ul class="dropdown-menu">
								<?
								while($sub = mysql_fetch_array($sub_query)) {
								?>
                                	<li><a tabindex="-1" href="subcategoria.php?k=<?=$sub['idProdutoSubCategoria']; ?>&idc=<?=$sub['IdProdutoCategoria']; ?>&n=<?=$sub['Descricao'];?>"><?=$sub["Descricao"];?></a></li>
								<?
								}
								?>
                               </ul>
							  <?
							 
							  }
							  ?>
                           	</li>
							<?
							}
							?>
                        </ul>
                	</li>
<li class="dropdown">
                    	<a href="moveis.php" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"><?=utf8_encode("M�veis"); ?> Planejados</a>
                        <ul class="dropdown-menu dropdown-megamenu">
                        	 <?
							 $prod_query = mysql_query("SELECT idProdutoCategoria, Descricao FROM MoveisCategoria ORDER BY Descricao");
							 while($prod = mysql_fetch_array($prod_query)) 
							{			
							?>
							<li class="dropdown">
                                <a tabindex="-1" href="moveiscategoria.php?k=<?=$prod['idProdutoCategoria']; ?>"><?=$prod["Descricao"];?></a>
	                        </li>
							<?
							}
							?>
                        </ul>
                	</li>
                      <li class="dropdown">
                    	<a href="novidades.php" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">Novidades</a>
                	</li>
                    <li class="dropdown">
                    	<a href="contato.php" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">Contato</a>
                	</li>
                    <li class="dropdown animate-click" data-animate="animated fadeInUp" style="z-index:500;">
                    	<a href="#" class="dropdown-toggle dropdown-form-toggle" data-toggle="dropdown"><i class="fa fa-search"></i></a>
                        <ul class="dropdown-menu dropdown-menu-user animate-wr">
                            <li id="dropdownForm">
                            	<div class="dropdown-form">
                                	<form class="form-default form-inline p-15" action="busca.php" method="get">
                                        <div class="input-group">
                                            <input type="text" name="p" id="p" class="form-control" placeholder="Busque o que deseja">
                                            <span class="input-group-btn">
                                                <script>

                                                    function Pesquisar() {
                                                        location.href = "busca.php?p=" + document.getElementById("p").value;
                                                    }
                                                </script>
                                                <button class="btn btn-two" type="button" onclick="Pesquisar();" >OK!</button>
                                            </span>
                                        </div>
                                    </form>
                                </div>
                            </li>
                        </ul>
                	</li>
              </ul>
                	<ul class="nav navbar-nav navbar-right hidden-xs">              		
               		<li>
                        <a href="https://www.facebook.com/RevestiDecor" class="social-link facebook" title="Facebook">
                        	<i class="fa fa-facebook"></i>
                        </a>
                    </li>
                    <li>
                        <a href="http://www.instagram.com/revestidecor" class="social-link instagram" title="Instagram">
                        	<i class="fa fa-instagram"></i>
                        </a>
                    </li>
               </ul>               
          </div><!--/.nav-collapse -->
        </div>
    </div>
</header>    

    
	
  

