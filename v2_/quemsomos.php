<?php header("Content-Type: text/html; charset=UTF-8",true) ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>Revesti Decorações e Móveis Planejados</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
     <!-- Required -->
    <link href="css/global-style.css" rel="stylesheet" type="text/css" media="screen">
    <link rel="icon" href="images/favicon.png" type="image/png">
</head>
<body>
<? include("controle/ConnFile.php"); ?>
<? include("menu.php"); ?>
<div class="wrapper">
	<!-- This section is only for demonstration purpose only. Just remove the div "divStyleSwitcher" -->
  <!-- Header: Logo and Main Nav -->


    </div>
</header>
     
    <section class="slice bg-5 p-15">
        <div class="w-section inverse">
            <div class="container">
                <div id="homepageCarousel" class="carousel carousel-1 slide color-two-l" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators hide">
                        <li data-target="#homepageCarousel" data-slide-to="0" class="active"></li>
                    </ol>
                    
                    <div class="carousel-inner" style="height:370px;">
					 <?
							 $banner_query = mysql_query("SELECT idBanner, Imagem FROM Banner WHERE Local = 'QuemSomos' ORDER BY idBanner DESC");
							$ativo = " active";						
							while($banner = mysql_fetch_array($banner_query)) 
							{			
						
							?>
                        <div class="item<?=$ativo; ?>">
                            <img src="img/produto/<?=$banner['Imagem'];?> " alt="">
                        </div>  
						 <? 
						 $ativo = "";
						 } ?>
                    </div>
                    
                    <!-- Controls -->
                    <a class="left carousel-control" href="#homepageCarousel" data-slide="prev">
                        <i class="fa fa-angle-left"></i>
                    </a>
                    <a class="right carousel-control" href="#homepageCarousel" data-slide="next">
                        <i class="fa fa-angle-right"></i>
                    </a>     
                </div>
            </div>
        </div>
    </section>
    
    <section class="slice bg-3">
        <div class="w-section inverse">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h3 class="section-title">Quem Somos</h3>
                        <p>Nascida da experiência de dois amigos, Cleyton Milat e Sérgio Monteiro, que já compartilhavam ideias e resultados há mais de 10 anos no ramo de revestimentos e decoração, a Revesti abriu suas portas ao público no dia 02/05/2008. A princípio, a empresa deu início às suas atividades em uma pequena sala comercial, situada no charmoso bairro do Cambuí.</p>
                      <p>Os trabalhos sempre foram executados sob a supervisão dos sócio-proprietarios, desde o fechamento do pedido, assessoramento referente ao serviço, instalação e entrega final ao cliente.<br>
                          Esta fórmula se mostrou um grande sucesso, fazendo com que a empresa, após um ano e meio de funcionamento, necessitasse de mais espaço e mudasse para o bairro Nova Campinas.</p>
                  </div>
                    <div class="col-md-6">
                        <h3 class="section-title">Parceria de Sucesso</h3>
                        <p>Juntamente com a mudança e a reinauguração, a Revesti passou a contar com a importante parceria da Hunter Douglas, se tornando assim, uma representante modelo da marca Luxaflex, que veio se juntar a outros grandes fabricantes como Duratex, Eucatex, Fademac, Placo entre outros.</p>
                        <p>Sendo assim, o trabalho prosseguiu sem cessar e o último passo se deu com a realização de um showroom com mais de 400,00m², no mais importante setor de decoração em Campinas, que possui a principal exposição de persianas e cortinas Luxaflex do interior de São Paulo, tornando-se assim, uma revenda Centurion, principal selo da marca.</p>
                        <p>Sempre com o compromisso de excelência, regido pela pontualidade, qualidade, seriedade e atendimento, a Revesti convida a todos a conhecerem a loja, assinada pela renomada arquiteta Adriana Bellão.</p>
                    </div>
                </div>
                <div class="row"></div>
            </div>
        </div>
    </section>
         
    <section class="slice bg-5">
        <div class="w-section inverse">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center">
                            <h4><i class="fa fa-quote-left fa-3x"></i></h4>
                            <h2>O sucesso é uma consequência e não um objetivo.</h2>
                            <div class="testimonial-text">
                               <p>
                                Gustavo Flaubert                            </p>
                            </div>
                            <span class="clearfix"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="slice bg-3">
  <div class="w-section inverse">
        <div class="container">
        	<h3 class="section-title">Marcas</h3>
            <div class="row">
<? require_once("marca.php");?>
            </div>
        </div>
    </div>
</section>  
<? require_once("rodape.php"); ?></div>

<!-- JavaScript -->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script type="text/javascript" src="js/jquery.easing.js"></script>

<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
<![endif]-->

<!-- Plugins -->
<script type="text/javascript" src="assets/hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
<script type="text/javascript" src="assets/masonry/masonry.js"></script>
<script type="text/javascript" src="assets/page-scroller/jquery.ui.totop.min.js"></script>
<script type="text/javascript" src="assets/mixitup/jquery.mixitup.js"></script>
<script type="text/javascript" src="assets/mixitup/jquery.mixitup.init.js"></script>
<script type="text/javascript" src="assets/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
<script type="text/javascript" src="assets/easy-pie-chart/jquery.easypiechart.js"></script>
<script type="text/javascript" src="assets/waypoints/waypoints.min.js"></script>
<script type="text/javascript" src="assets/sticky/jquery.sticky.js"></script>
<script type="text/javascript" src="js/jquery.wp.custom.js"></script>
<script type="text/javascript" src="js/jquery.wp.switcher.js"></script>
</body>
</html>